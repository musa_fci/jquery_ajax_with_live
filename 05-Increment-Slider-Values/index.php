<html>
    <head>
        <title>Increment Slider Values</title>
        <link href="jquery-ui.min.css" type="text/css" rel="stylesheet">
        <script src="jquery.js"></script>
        <script src="jquery-ui.min.js"></script>
        <script>
            $(document).ready(function () {
                $("#slider").slider({
                    value: 100,
                    min: 0,
                    max: 500,
                    step: 50,
                    slide: function (event, ui) {
                        $("#amount").val("Tk. " + ui.value);
                    }
                });
                $("#amount").val("Tk." + $("#slider").slider("value"));
            });

        </script>
    </head>
    <style>
        .wrapper{width: 600px;min-height: 500px;height: auto;padding: 30px;margin: 20px auto;background: #DDD;}
        .heading{padding: 15px 0px;text-align: center;}    
        .error_form{color: red;}
    </style>
    <body>
        <div class="wrapper">
            <h2 class="heading">Increment Slider Values</h2>
            <label for="amount">select price</label>
            <input type="text" id="amount" readonly="" style="font-weight: bold;color: green;margin-bottom: 50px;">
            <div id="slider"></div>
        </div>
    </body>
</html>