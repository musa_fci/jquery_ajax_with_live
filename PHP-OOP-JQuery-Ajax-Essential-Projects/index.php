<?php include 'inc/header.php'; ?>
<h2>Topics: Project List</h2>
<div class="content">
    <div class="topics">
        <ul>
            <li><a href="user.php" target="_blank">01-AJAX:: Username Availability</a></li>
            <li><a href="textbox.php" target="_blank">02-AJAX:: Autocomplete Textbox</a></li>
            <li><a href="password.php" target="_blank">03-AJAX:: Show Password Button</a></li>
            <li><a href="refresh.php" target="_blank">04-AJAX:: Auto Refresh Div Content</a></li>
            <li><a href="livesearch.php" target="_blank">05-AJAX:: Live Data Search</a></li>
            <li><a href="autosave.php" target="_blank">06-AJAX:: Auto Save Data</a></li>
        </ul>
    </div>
</div>
<?php include 'inc/footer.php'; ?>