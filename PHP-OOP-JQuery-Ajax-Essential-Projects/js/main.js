$(document).ready(function () {

//Username Availability
    $("#username").blur(function () {
        var userName = $(this).val();
        $.ajax({
            url: "check/checkuser.php",
            method: "POST",
            data: {username: userName},
            datatype: "text",
            success: function (data) {
                $("#userstatus").html(data);
            }
        });
    });
    //Autocomplete Textbox
    $("#skill").keyup(function () {
        var skill = $(this).val();
        if (skill != "") {
            $.ajax({
                url: "check/checkskill.php",
                method: "POST",
                data: {skill: skill},
                success: function (data) {
                    $("#statusskill").fadeIn();
                    $("#statusskill").html(data);
                }
            });
        }

        $(document).on('click', 'li', function () {
            $("#skill").val($(this).text());
            $("#statusskill").fadeOut();
        });
    });
    //Show Password Button
    $("#showpassword").on('click', function () {
        var pass = $("#password");
        var passFieldType = pass.attr('type');
        if (passFieldType == 'password') {
            pass.attr('type', 'text');
            $(this).text('hide password');
        } else {
            pass.attr('type', 'password');
            $(this).text('show password');
        }
    });
    //Auto Refresh Div Content
    $("#autosubmit").click(function () {
        var content = $("#body").val();
        if ($.trim(content) != "") {
            $.ajax({
                url: "check/checkrefresh.php",
                method: "POST",
                data: {body: content},
                datatype: "text",
                success: function (data) {
                    $("#body").val("");
                }
            });
            return false;
        }
    });
    setInterval(function () {
        $("#autostatus").load("check/getrefresh.php").fadeIn("slow");
    }, 2000);
    
    
    //Live Data Search
    $("#livesearch").keyup(function () {
        var live = $(this).val();
        if (live != "") {
            $.ajax({
                url: "check/livesearch.php",
                method: "POST",
                data: {livesearch: live},
                datatype: "text",
                success: function (data) {
                    $("#statuslive").html(data);
                }
            });
        } else {
            $("#statuslive").html("");
        }
    });
    //Auto Save Data
    function autoSave() {
        var content = $("#content").val();
        var contentid = $("#contentid").val();
        if (content != "") {
            $.ajax({
                url: "check/autosave.php",
                method: "POST",
                data: {content: content, contentid: contentid},
                datatype: "text",
                success: function (data) {
                    if (data != '') {
                        $("#contentid").val(data);
                    }
                    $("#statussave").text("Content Save as Draft");
                    setInterval(function () {
                        $("#statussave").text("");
                    }, 2000);
                }
            });
        }
    }

    setInterval(function () {
        autoSave();
    }, 10000);


});
