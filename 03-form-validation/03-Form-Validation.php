<html>
    <head>
        <title>Form Validation</title>
    </head>
    <style>
        .wrapper{width: 600px;min-height: 500px;height: auto;margin: 20px auto;background: #DDD;}
        .heading{padding: 15px 0px;text-align: center;}    
        .error_form{color: red;}
    </style>
    <body>
        <div class="wrapper">
            <h2 class="heading">Form Validation</h2>
            <form method="post" action="msg.php" id="myform">
                <table>
                    <tr>
                        <td>Username</td>
                        <td>:</td>
                        <td>
                            <input type="text" id="username">
                        </td>
                        <td><span class="error_form" id="username_error_msg"></span></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td>:</td>
                        <td>
                            <input type="password" id="password">
                        </td>
                        <td><span class="error_form" id="password_error_msg"></span></td>
                    </tr>
                    <tr>
                        <td>Retype Password</td>
                        <td>:</td>
                        <td>
                            <input type="password" id="password_again">
                        </td>
                        <td><span class="error_form" id="password_again_error_msg"></span></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td>
                            <input type="email" id="email">
                        </td>
                        <td><span class="error_form" id="email_error_msg"></span></td>
                    </tr>
                    <tr>    
                        <td colspan="2"></td>
                        <td>
                            <button id="btn">submit</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
         <script src="jquery-migrate-1.4.1.min.js"></script>
         <script src="custom.js"></script>

    </body>
</html>