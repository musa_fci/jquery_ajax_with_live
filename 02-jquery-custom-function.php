<html>
    <head>
        <title>jQuery Custom Function</title>
        <script src="jquery-migrate-1.4.1.min.js"></script>
        <script>
            $(document).ready(function () {

                $("#btn").on('click', function (e) {
                    var isValid = true;
                    $(".required").each(function () {
                        if ($(this).val() == '') {
                            isValid = false;
                            $(this).css({
                                "background": "#FFCECE",
                                "border": "3px solid #FFCECE"
                            });
                        } else {
                            $(this).css({
                                "background": "",
                                "border": ""
                            });
                        }
                    });
                    if (isValid == false) {
                        e.preventDefault();
                    } else {
                        clear()
                        document.getElementById("msg").innerHTML = "OK";
                        return false;
                    }

                });
            });

            function clear() {
                $("#myform :input").each(function () {
                    $(this).val("");
                });
            }

        </script>
    </head>
    <style>
        .wrapper{width: 600px;min-height: 500px;height: auto;margin: 20px auto;background: #DDD;}
        .heading{padding: 15px 0px;text-align: center;}        
    </style>
    <body>
        <div class="wrapper">
            <h2 class="heading">jQuery Custom Function</h2>
            <form method="post" action="" id="myform">
                <p id="msg"></p>
                <table>
                    <tr>
                        <td>Name</td>
                        <td>:</td>
                        <td>
                            <input type="text" class="required">
                        </td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td>
                            <input type="text" class="required">
                        </td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td>:</td>
                        <td>
                            <input type="text" class="required">
                        </td>
                    </tr>
                    <tr>    
                        <td colspan="2"></td>
                        <td>
                            <button id="btn">submit</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>

    </body>
</html>